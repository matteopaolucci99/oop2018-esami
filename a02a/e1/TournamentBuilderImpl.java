package a02a.e1;

import java.util.EnumMap;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import a02a.e1.Tournament.Result;
import a02a.e1.Tournament.Type;

public class TournamentBuilderImpl implements TournamentBuilder {
	static {
		MAXPOINTS = new EnumMap<>(Type.class) {{
			this.put(Type.MAJOR, 2500);
			this.put(Type.ATP1000, 1000);
			this.put(Type.ATP500, 500);
			this.put(Type.ATP250, 250);
		}};
		
		SCALINGFACTORS = new EnumMap<>(Result.class) {{
			this.put(Result.WINNER, 1.0);
			this.put(Result.FINALIST, 0.5);
			this.put(Result.SEMIFINALIST, 0.2);
			this.put(Result.QUARTERFINALIST, 0.1);
			this.put(Result.PARTICIPANT, 0.0);
		}};
	}
	
	private static final EnumMap<Type, Integer> MAXPOINTS;
	private static final EnumMap<Result, Double> SCALINGFACTORS;
	private Type type;
	private String name;
	private Map<String, Integer> priorRanking;
	private Map<String, Integer> finalRanking;
	private Map<String, Result> classific;
	private boolean tournamentBuilt;
	
	public TournamentBuilderImpl() {
		this.classific = new HashMap<>();
		this.tournamentBuilt = false;
	}

	@Override
	public TournamentBuilder setType(Type type) {
		check(Objects.nonNull(this.name));
		this.type = Objects.requireNonNull(type);
		return this;
	}

	@Override
	public TournamentBuilder setName(String name) {
		this.name = Objects.requireNonNull(name);
		return this;
	}

	@Override
	public TournamentBuilder setPriorRanking(Map<String, Integer> ranking) {
		this.priorRanking = new HashMap<>(ranking);
		this.finalRanking = new HashMap<>(ranking);
		return this;
	}

	@Override
	public TournamentBuilder addResult(String player, Result result) {
		Objects.nonNull(this.type);
		Objects.nonNull(this.priorRanking);
		check(!this.classific.containsKey(player));
		if(result.equals(Result.WINNER)) {
			check(this.classific.entrySet().stream().map(Map.Entry::getValue).noneMatch(res -> res.equals(Result.WINNER)));
		}
		this.classific.put(player, result);
		this.finalRanking.put(player, (this.finalRanking.containsKey(player) ? this.finalRanking.get(player) : 0)
				+ (int) (MAXPOINTS.get(this.type) * SCALINGFACTORS.get(result)));
		return this;
	}

	@Override
	public Tournament build() {
		check(!this.tournamentBuilt);
		this.tournamentBuilt = true;
		return new Tournament() {
			private Type type = TournamentBuilderImpl.this.type;
			private String name = TournamentBuilderImpl.this.name;
			private Map<String, Integer> priorRanking = new HashMap<>(TournamentBuilderImpl.this.priorRanking);
			private Map<String, Integer> finalRanking = new HashMap<>(TournamentBuilderImpl.this.finalRanking);
			private Map<String, Result> classific = new HashMap<>(TournamentBuilderImpl.this.classific);			
			
			@Override
			public String winner() {
				return this.classific.entrySet().stream()
										   .filter(e -> e.getValue().equals(Result.WINNER))
										   .map(Map.Entry::getKey)
										   .findAny()
										   .get();
			}
			
			@Override
			public Map<String, Integer> resultingRanking() {
				return this.finalRanking;
			}
			
			@Override
			public List<String> rank() {
				return this.finalRanking.entrySet().stream()
											  .sorted((e1, e2) -> Integer.compare(e1.getValue(), e2.getValue()) * -1)
											  .map(Map.Entry::getKey)
											  .collect(Collectors.toList());
			}
			
			@Override
			public Map<String, Integer> initialRanking() {
				return this.priorRanking;
			}
			
			@Override
			public Type getType() {
				return this.type;
			}
			
			@Override
			public Optional<Result> getResult(String player) {
				return this.classific.containsKey(player) ? Optional.of(this.classific.get(player)) : Optional.empty();
			}
			
			@Override
			public String getName() {
				return this.name;
			}
		};
	}
	
	private static void check(boolean expression) {
		if(expression == false) {
			throw new IllegalStateException();
		}
	}

}
