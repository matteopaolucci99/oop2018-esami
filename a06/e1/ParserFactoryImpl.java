package a06.e1;

import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Queue;
import java.util.Set;
import java.util.stream.Stream;

public class ParserFactoryImpl implements ParserFactory {

	@Override
	public Parser one(String token) {
		return new Parser() {
			private String mainToken = token;
			private boolean tokenProduced = false;
			
			@Override
			public void reset() {
				this.tokenProduced = false;
			}
			
			@Override
			public boolean inputCompleted() {
				return this.tokenProduced;
			}
			
			@Override
			public boolean acceptToken(String token) {
				if(this.inputCompleted()) {
					return false;
				} else if (this.mainToken.equals(token)) {
					this.tokenProduced = true;
					return true;
				} else {
					return false;
				}
			}
		};
	}

	@Override
	public Parser many(String token, int elemCount) {
		return new Parser() {
			Iterator<String> iterator = Stream.generate(() -> token).limit(elemCount).iterator();
			String cur;
			
			@Override
			public void reset() {
				this.iterator = Stream.generate(() -> token).limit(elemCount).iterator();
			}
			
			@Override
			public boolean inputCompleted() {
				return !this.iterator.hasNext();
			}
			
			@Override
			public boolean acceptToken(String token) {
				if(Objects.isNull(this.cur)) {
					this.cur = this.iterator.next();
				}				
				if(this.cur.equals(token)) {
					if(this.iterator.hasNext()) {
						this.cur = this.iterator.next();
					}
					return true;
				} else {
					return false;
				}
			}
		};
	}

	@Override
	public Parser oneOf(Set<String> set) {
		return new Parser() {
			private boolean tokenProduced = false;
			@Override
			public void reset() {
				this.tokenProduced = false;				
			}
			
			@Override
			public boolean inputCompleted() {
				return this.tokenProduced;
			}
			
			@Override
			public boolean acceptToken(String token) {
				if(set.contains(token) && !this.inputCompleted()) {
					this.tokenProduced = true; 
					return true;
				}
				return false;
			}
		};
	}

	@Override
	public Parser sequence(String token1, String token2) {
		return new Parser() {
			Queue<String> list = new LinkedList<>() {{
				this.addLast(token1);
				this.addLast(token2);
			}};
			
			@Override
			public void reset() {
				this.list = new LinkedList<>() {{
					this.addLast(token1);
					this.addLast(token2);
				}};
			}
			
			@Override
			public boolean inputCompleted() {
				return this.list.isEmpty();
			}
			
			@Override
			public boolean acceptToken(String token) {
				if(!this.inputCompleted()) {
					if(this.list.peek().contentEquals(token)) {
						this.list.poll();
						return true;
					}
				}
				return false;
			}
		};
	}

	@Override
	public Parser fullSequence(String begin, Set<String> elem, String separator, String end, int elemCount) {
		// TODO Auto-generated method stub
		return null;
	}

}
