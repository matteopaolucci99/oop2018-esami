package a06.e2;

import java.util.HashMap;
import java.util.Map;

public class LogicImpl implements Logic {
	private Map<Integer, Integer> values;
	
	public LogicImpl(int size) {
		this.values = new HashMap<>();
		for(int i = 0; i < size; i++) {
			this.values.put(i, 1);
		}
	}

	@Override
	public void hit(int index) {
		int sum = 0;
		for(int i = 0; i <= index; i++) {
			sum += this.values.get(i);
		}
		this.values.put(index, sum);
	}

	@Override
	public void reset() {
		for(var entry : this.values.entrySet()) {
			values.put(entry.getKey(), 1);
		}
	}

	@Override
	public int getValue(int index) {
		return this.values.get(index);
	}
}
