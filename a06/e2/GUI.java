package a06.e2;

import java.awt.FlowLayout;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JFrame;


public class GUI extends JFrame{
	private int size;
	private Logic logic;
	private Map<JButton, Integer> buttons;
	
	public GUI(int size){
		this.size = size;
		this.logic = new LogicImpl(this.size);
		this.buttons = new HashMap<>();
		final JButton reset = new JButton("Reset");
		ActionListener ac = e -> {
			final JButton jb = (JButton)e.getSource();
			this.logic.hit(this.buttons.get(jb));
			jb.setEnabled(false);
			this.redraw();
		};
		
		this.setSize(500, 100);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.getContentPane().setLayout(new FlowLayout());
		
		for(int i = 0; i < this.size; i++) {
			final JButton jb = new JButton("1");
			jb.addActionListener(ac);
			this.getContentPane().add(jb);
			this.buttons.put(jb, i);
		}
		
		this.getContentPane().add(reset);
		reset.addActionListener(e -> {
			this.logic.reset();
			for(var entry : this.buttons.entrySet()) {
				entry.getKey().setEnabled(true);
			}
			this.redraw();
		});
		
		this.redraw();
	}

	private void redraw() {
		for(var e : this.buttons.entrySet()) {
			e.getKey().setText(String.valueOf(this.logic.getValue(e.getValue())));
		}
		this.setVisible(true);
	}	
}
