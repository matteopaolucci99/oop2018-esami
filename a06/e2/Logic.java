package a06.e2;

public interface Logic {
	void hit(int index);
	
	void reset();
	
	int getValue(int index);
}
