package a04.e1;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class IntegerIteratorsFactoryImpl implements IntegerIteratorsFactory {

	@Override
	public SimpleIterator<Integer> empty() {
		return () -> Optional.empty();
	}

	@Override
	public SimpleIterator<Integer> fromList(List<Integer> list) {
		Iterator<Integer> iterator = list.iterator();
		return () -> iterator.hasNext() ? Optional.of(iterator.next()) : Optional.empty();
	}

	@Override
	public SimpleIterator<Integer> random(int size) {
		Random random = new Random();
		Iterator<Integer> iterator = Stream.generate(() -> random.nextInt(size))
										   .limit(size)
										   .iterator();
		return () -> iterator.hasNext() ? Optional.of(iterator.next()) : Optional.empty();
	}

	@Override
	public SimpleIterator<Integer> quadratic() {
		Iterator<Integer> iterator = Stream.iterate(1, i -> i + 1)
										   .flatMap(i -> Collections.nCopies(i, i).stream())
										   .iterator();
		return () -> Optional.of(iterator.next());
	}

	@Override
	public SimpleIterator<Integer> recurring() {
		Iterator<Integer> iterator = IntStream.iterate(0, i -> i + 1)
										      .flatMap(i -> IntStream.rangeClosed(0, i))
										      .iterator();
		return () -> Optional.of(iterator.next());
	}
}