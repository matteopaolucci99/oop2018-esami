package a01a.e1;

import java.util.Iterator;
import java.util.Optional;

public abstract class AbstactSplitIterator<X> implements SplitIterator<X> {
	private Iterator<X> iterator;

	public AbstactSplitIterator(Iterator<X> iterator) {
		this.iterator = iterator;
	}
	
	@Override
	public Optional<X> next() {
		return iterator.hasNext() ? Optional.of(this.iterator.next()) : Optional.empty();
	}
	
	abstract public SplitIterator<X> split();
	
	protected Iterator<X> getIterator() {
		return iterator;
	}

	protected void setIterator(Iterator<X> iterator) {
		this.iterator = iterator;
	}
}
