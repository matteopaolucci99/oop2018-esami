package a01a.e1;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.IntStream;
import java.util.stream.Stream;


public class SplitIteratorFactoryImpl implements SplitIteratorFactory {

	@Override
	public SplitIterator<Integer> fromRange(int start, int stop) {
		Iterator<Integer> iterator = IntStream.range(start, stop + 1).iterator();
		return new AbstactSplitIterator<Integer>(iterator) { 
			@Override
			public SplitIterator<Integer> split() {
				int elem = this.getIterator().next();
				this.setIterator(IntStream.range(((stop - elem) / 2) + 1, stop + 1).iterator());
				return fromRange(elem, (stop - elem) / 2);
			}
		};
	}

	@Override
	public SplitIterator<Integer> fromRangeNoSplit(int start, int stop) {
		Iterator<Integer> iterator = IntStream.range(start, stop + 1).iterator();
		return new AbstactSplitIterator<Integer>(iterator) {
			@Override
			public SplitIterator<Integer> split() {
				throw new UnsupportedOperationException();
			}
		};
	}

	@Override
	public <X> SplitIterator<X> fromList(List<X> list) {
		Iterator<X> iterator = list.iterator();
		return new AbstactSplitIterator<X>(iterator) {
			private int cur = 0;

			@Override
			public SplitIterator<X> split() {
				List<X> list1 = new LinkedList<>();
				int size = list.size() - cur;
				while(cur <= (size / 2)) {
					list1.add(this.getIterator().next());
					cur++;
				}
				return fromList(list1);
			}
			
			@Override
			public Optional<X> next() {
				cur++;
				return super.next();
			}
		};
	}

	@Override
	public <X> SplitIterator<X> fromListNoSplit(List<X> list) {
		Iterator<X> iterator = list.iterator();
		return new AbstactSplitIterator<X>(iterator) {
			@Override
			public SplitIterator<X> split() {
				throw new UnsupportedOperationException();
			}
		};
	}

	@Override
	public <X> SplitIterator<X> excludeFirst(SplitIterator<X> si) {
		si.next();
		return new SplitIterator<X>() {
			@Override
			public Optional<X> next() {
				return si.next();
			}

			@Override
			public SplitIterator<X> split() {
				return si.split();
			}
		};
	}

	@Override
	public <X, Y> SplitIterator<Y> map(SplitIterator<X> si, Function<X, Y> mapper) {
		return new SplitIterator<Y>() {
			@Override
			public Optional<Y> next() {
				Optional<X> val = si.next();
				return val.isPresent() ? Optional.of(mapper.apply(val.get())) : Optional.empty();
			}

			@Override
			public SplitIterator<Y> split() {
				return map(si.split(), mapper);
			}
		};
	}

}
