package a01a.e2;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class GUI extends JFrame {
	private final Logic game;
	private final Map<JButton, Pair<Integer, Integer>> buttons;

	public GUI() {
		this.game = new LogicImpl();
		final JPanel panel = new JPanel(new GridLayout(5, 5));
		this.buttons = new HashMap<>();
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setSize(500, 500);
		this.getContentPane().add(BorderLayout.CENTER, panel);

		ActionListener al = (e) -> {
			final JButton bt = (JButton) e.getSource();
			if(game.hit(this.buttons.get(bt).getX(), this.buttons.get(bt).getY())) {
				System.exit(0);
			}
			this.redraw();
		};

		for (int i = 0; i < 5; i++) {
			for (int j = 0; j < 5; j++) {
				final JButton jb = new JButton();
				jb.addActionListener(al);
				this.buttons.put(jb, new Pair<>(i, j));
				panel.add(jb);
			}
		}
		this.redraw();
	}

	private void redraw() {
		this.buttons.entrySet().forEach(e -> {
			if(game.hasKnight(e.getValue().getX(), e.getValue().getY())) {
				e.getKey().setText("K");
			} else if (game.hasPawn(e.getValue().getX(), e.getValue().getY())) {
				e.getKey().setText("*");
			} else {
				e.getKey().setText(" ");
			}
		});
		this.setVisible(true);
	}
}