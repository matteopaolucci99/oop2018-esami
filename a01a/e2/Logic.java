package a01a.e2;

public interface Logic {
	boolean hit(int x, int y);
	
	boolean hasPawn(int x, int y);
	
	boolean hasKnight(int x, int y);
}
