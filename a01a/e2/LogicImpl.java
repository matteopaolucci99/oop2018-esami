package a01a.e2;

import java.util.Random;

public class LogicImpl implements Logic {
	private Pair<Integer, Integer> knightPosition;
	private final Pair<Integer, Integer> pawnPosition;
	private static final Random RANDOM = new Random();

	public LogicImpl() {
		this.knightPosition = new Pair<Integer, Integer>(RANDOM.nextInt(5), RANDOM.nextInt(5));
		this.pawnPosition = new Pair<Integer, Integer>(RANDOM.nextInt(5), RANDOM.nextInt(5));
	}

	public boolean hit(int x, int y) {
		if (Math.abs(this.knightPosition.getX() - x) == 2 && Math.abs(this.knightPosition.getY() - y) == 1) {
			this.knightPosition = new Pair<>(x, y);
		} else if (Math.abs(this.knightPosition.getX() - x) == 1 && Math.abs(this.knightPosition.getY() - y) == 2) {
			this.knightPosition = new Pair<>(x, y);
		}
		if (this.knightPosition.equals(this.pawnPosition)) {
			return true;
		}
		return false;
	}

	@Override
	public boolean hasPawn(int x, int y) {
		return this.pawnPosition.equals(new Pair<>(x, y));
	}

	@Override
	public boolean hasKnight(int x, int y) {
		return this.knightPosition.equals(new Pair<>(x, y));
	}
}