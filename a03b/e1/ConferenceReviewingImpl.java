package a03b.e1;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class ConferenceReviewingImpl implements ConferenceReviewing {

	private Map<Integer, List<Map<Question, Integer>>> articles;
	
	public ConferenceReviewingImpl() {
		this.articles = new HashMap<>();
	}

	@Override
	public void loadReview(int article, Map<Question, Integer> scores) {
		if(!this.articles.containsKey(article)) {
			this.articles.put(article, new LinkedList<>());
		}
		this.articles.get(article).add(new HashMap<>(scores));
	}

	@Override
	public void loadReview(int article, int relevance, int significance, int confidence, int fin) {
		if(!this.articles.containsKey(article)) {
			this.articles.put(article, new LinkedList<>());
		}
		 this.articles.get(article).add( new HashMap<>() {{
			 this.put(Question.CONFIDENCE, confidence);
			 this.put(Question.RELEVANCE, relevance);
			 this.put(Question.FINAL, fin);
			 this.put(Question.SIGNIFICANCE, significance);
		 }});
	}

	@Override
	public List<Integer> orderedScores(int article, Question question) {
		return this.articles.get(article).stream()
										 .flatMap(m -> m.entrySet().stream())
										 .filter(e -> e.getKey().equals(question))
										 .map(Map.Entry::getValue)
										 .sorted()
										 .collect(Collectors.toList());
	}

	@Override
	public double averageFinalScore(int article) {
		return this.articles.get(article).stream()
										  .flatMap(m -> m.entrySet().stream())
										  .filter(e -> e.getKey().equals(Question.FINAL))
										  .mapToDouble(Map.Entry::getValue)
										  .average()
										  .getAsDouble();
	}

	@Override
	public Set<Integer> acceptedArticles() {
		return this.articles.entrySet().stream()
									   .filter(e -> averageFinalScore(e.getKey()) > 5)
									   .filter(e -> e.getValue().stream()
											   					.flatMap(m -> m.entrySet().stream())
											   					.filter(e1 -> e1.getKey().equals(Question.RELEVANCE))
											   					.anyMatch(e1 -> e1.getValue() >= 8))
									   .map(Map.Entry::getKey)
									   .collect(Collectors.toSet());
	}

	@Override
	public List<Pair<Integer, Double>> sortedAcceptedArticles() {
		return this.articles.entrySet().stream()
									   .filter(e -> this.acceptedArticles().contains(e.getKey()))
									   .map(e -> new Pair<>(e.getKey(), this.averageFinalScore(e.getKey())))
									   .sorted((e1, e2) -> Double.compare(e1.getY(), e2.getY()))
									   .collect(Collectors.toList());
	}

	@Override
	public Map<Integer, Double> averageWeightedFinalScoreMap() {
		return this.articles.entrySet().stream()
									   .collect(Collectors.toMap(Map.Entry::getKey, e -> (e.getValue().stream()
																			   					   	  .flatMap(m -> m.entrySet().stream())
																			   					   	  .filter(e3 -> e3.getKey().equals(Question.RELEVANCE))
																			   					   	  .mapToDouble(Map.Entry::getValue)
																			   					   	  .sum()
																			   					   	  * 
																			   					   	  e.getValue().stream()
																			   					   	  .flatMap(m -> m.entrySet().stream())
																			   					   	  .filter(e3 -> e3.getKey().equals(Question.FINAL))
																			   					   	  .mapToDouble(Map.Entry::getValue)
																			   					   	  .sum()) / 10));									   
	}

}
