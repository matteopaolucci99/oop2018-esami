package a01b.e1;

import java.util.Optional;

import a01b.e1.ExamResult.Kind;

public class ExamResultFactoryImpl implements ExamResultFactory {

	@Override
	public ExamResult failed() {
		return new ExamResult() {
			
			@Override
			public Kind getKind() {
				return Kind.FAILED;
			}
			
			@Override
			public Optional<Integer> getEvaluation() {
				return Optional.empty();
			}
			
			@Override
			public boolean cumLaude() {
				return false;
			}
			
			@Override
			public String toString() {
				return "FAILED";
			}
		};
	}

	@Override
	public ExamResult retired() {
		return new ExamResult() {
			
			@Override
			public Kind getKind() {
				return Kind.RETIRED;
			}
			
			@Override
			public Optional<Integer> getEvaluation() {
				return Optional.empty();
			}
			
			@Override
			public boolean cumLaude() {
				return false;
			}
			
			@Override
			public String toString() {
				return "RETIRED";
			}
		};
	}

	@Override
	public ExamResult succeededCumLaude() {
		return new ExamResult() {
			
			@Override
			public Kind getKind() {
				return Kind.SUCCEEDED;
			}
			
			@Override
			public Optional<Integer> getEvaluation() {
				return Optional.of(30);
			}
			
			@Override
			public boolean cumLaude() {
				return true;
			}
			
			@Override
			public String toString() {
				// TODO Auto-generated method stub
				return "SUCCEEDED(30L)";
			}
		};
	}

	@Override
	public ExamResult succeeded(int evaluation) {
		if(evaluation < 18  || evaluation > 30) {
			throw new IllegalArgumentException();
		}
		return new ExamResult() {
			
			@Override
			public Kind getKind() {
				return Kind.SUCCEEDED;
			}
			
			@Override
			public Optional<Integer> getEvaluation() {
				return Optional.of(evaluation);
			}
			
			@Override
			public boolean cumLaude() {
				return false;
			}
			
			@Override
			public String toString() {
				return "SUCCEEDED(" + evaluation + ")";
			}
		};
	}

}
