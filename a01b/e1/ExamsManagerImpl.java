package a01b.e1;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

public class ExamsManagerImpl implements ExamsManager {
	private Map<String, Optional<Map<String, ExamResult>>> calls;
	
	public ExamsManagerImpl() {
		this.calls = new HashMap<>();
	}

	@Override
	public void createNewCall(String call) {
		if(this.calls.containsKey(call)) {
			throw new IllegalArgumentException();
		}
		this.calls.put(call, Optional.empty());
	}

	@Override
	public void addStudentResult(String call, String student, ExamResult result) {
		if(this.calls.get(call).isEmpty()) {
			this.calls.put(call, Optional.of(new HashMap<>()));
		} 
		if(this.calls.get(call).get().containsKey(student)) {
			throw new IllegalArgumentException();
		}
		this.calls.get(call).get().put(student, result);
	}

	@Override
	public Set<String> getAllStudentsFromCall(String call) {
		return this.calls.get(call).get().keySet();
	}

	@Override
	public Map<String, Integer> getEvaluationsMapFromCall(String call) {
		return 	this.calls.get(call).get().entrySet().stream()
													 .filter(e -> e.getValue().getEvaluation().isPresent())
													 .collect(Collectors.toMap(Map.Entry::getKey, e -> e.getValue().getEvaluation().get()));
	}

	@Override
	public Map<String, String> getResultsMapFromStudent(String student) {
		return this.calls.entrySet().stream()
									.filter(m -> m.getValue().isPresent())
									.filter(m -> m.getValue().get().containsKey(student))
									.collect(Collectors.toMap(Map.Entry::getKey, e -> e.getValue().get().get(student).toString()));
	}

	@Override
	public Optional<Integer> getBestResultFromStudent(String student) {
		return this.calls.entrySet().stream()
								    .map(Map.Entry::getValue)
								    .filter(Optional::isPresent)
								    .map(Optional::get)
								    .filter(m -> m.containsKey(student))
								    .flatMap(m -> m.entrySet().stream())
								    .filter(e -> e.getKey().equals(student))
								    .map(Map.Entry::getValue)
								    .map(ExamResult::getEvaluation)
								    .filter(Optional::isPresent)
								    .map(Optional::get)
								    .max(Integer::compare);
	}

}
