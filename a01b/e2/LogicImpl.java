package a01b.e2;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class LogicImpl implements Logic {
	private boolean bishopHit;
	private Pair<Integer, Integer> bishopPosition;
	private final Map<Pair<Integer, Integer>, Boolean> squaresDisabled;
	private static final Random RANDOM = new Random();

	public LogicImpl() {
		this.bishopHit = false;
		this.bishopPosition = new Pair<>(RANDOM.nextInt(5), RANDOM.nextInt(5));
		this.squaresDisabled = new HashMap<>();
		for (int i = 0; i < 5; i++) {
			for (int j = 0; j < 5; j++) {
				this.squaresDisabled.put(new Pair<>(i, j), false);
			}
		}
	}

	@Override
	public void hit(int x, int y) {
		Pair<Integer, Integer> coor = new Pair<>(x, y);
		if (this.bishopPosition.equals(coor) && !bishopHit) {
			bishopHit = true;
			computeDisabledPosition();
		} else if (bishopHit && !this.isDisabled(coor.getX(), coor.getY())) {
			this.bishopPosition = coor;
			this.bishopHit = false;
			this.squaresDisabled.forEach((k, v) -> this.squaresDisabled.put(k, false));
		}
	}

	private void computeDisabledPosition() {
		this.squaresDisabled.forEach((k, v) -> {
			this.squaresDisabled.put(k, 
					(Math.abs(k.getX() - bishopPosition.getX()) == Math.abs(k.getY() - bishopPosition.getY())) ? false : true);
		});
	}

	@Override
	public boolean isBishopPresent(int x, int y) {
		return this.bishopPosition.equals(new Pair<>(x, y));
	}

	@Override
	public boolean isDisabled(int x, int y) {
		return this.squaresDisabled.get(new Pair<>(x, y));
	}

}
