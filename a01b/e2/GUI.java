package a01b.e2;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.*;

public class GUI extends JFrame {

	private final Map<JButton, Pair<Integer, Integer>> buttons = new HashMap<>();
	private final Logic game = new LogicImpl();

	public GUI() {
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setSize(500, 500);

		JPanel panel = new JPanel(new GridLayout(5, 5));
		this.getContentPane().add(BorderLayout.CENTER, panel);

		ActionListener al = (e) -> {
			final JButton bt = (JButton) e.getSource();
			final Pair<Integer, Integer> coor = this.buttons.get(bt);
			this.game.hit(coor.getX(), coor.getY());
			this.redraw();
		};

		for (int i = 0; i < 5; i++) {
			for (int j = 0; j < 5; j++) {
				final JButton jb = new JButton(" ");
				jb.addActionListener(al);
				this.buttons.put(jb, new Pair<>(i, j));
				panel.add(jb);				
			}
		}
		this.redraw();
	}

	private void redraw() {
		for(var val : this.buttons.entrySet()) {
			Pair<Integer, Integer> coor = val.getValue();
			if(game.isBishopPresent(coor.getX(), coor.getY())) {
				val.getKey().setText("B");
			} else {
				val.getKey().setText(" ");
				if (game.isDisabled(coor.getX(), coor.getY())) {
					val.getKey().setEnabled(false);
				} else {
					val.getKey().setEnabled(true);
				}
			}
		}
		this.setVisible(true);
	}
	
	

}
