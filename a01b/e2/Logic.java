package a01b.e2;

public interface Logic {
	void hit(int x, int y);
	
	boolean isBishopPresent(int x, int y);
	
	boolean isDisabled(int x, int y);
}
