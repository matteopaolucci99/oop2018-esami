package a05.e1;

import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.function.UnaryOperator;
import java.util.stream.Stream;

public class PowerIteratorsFactoryImpl implements PowerIteratorsFactory {

	@Override
	public PowerIterator<Integer> incremental(int start, UnaryOperator<Integer> successive) {
		Iterator<Integer> iterator = Stream.iterate(start, successive).iterator();
		return new PowerIteratorImpl<>(iterator);
	}

	@Override
	public <X> PowerIterator<X> fromList(List<X> list) {
		return new PowerIteratorImpl<X>(list.iterator());
	}

	@Override
	public PowerIterator<Boolean> randomBooleans(int size) {
		Random random = new Random();
		Iterator<Boolean> iterator = Stream.generate(() -> random.nextBoolean()).limit(size).iterator();
		return new PowerIteratorImpl<>(iterator);
	}

}
