package a05.e1;

import java.util.Deque;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

public class PowerIteratorImpl<T> implements PowerIterator<T> {
	private List<T> allSoFar;
	private Deque<T> reversed;
	private Iterator<T> iterator;
	
	public PowerIteratorImpl(Iterator<T> iterator) {
		this.allSoFar = new LinkedList<T>();
		this.reversed = new LinkedList<T>();
		this.iterator = iterator;
	}
	
	@Override
	public Optional<T> next() {
		if(iterator.hasNext()) {
			T elem = iterator.next();
			this.reversed.addFirst(elem);
			this.allSoFar.add(elem);
			return Optional.of(elem);
		}
		return Optional.empty();
	}
	
	public List<T> allSoFar() {
		return new LinkedList<T>(this.allSoFar);
	}
	
	@Override
	public PowerIterator<T> reversed() {
		return new PowerIteratorImpl<>(new LinkedList<T>(this.reversed).iterator());
	}
}
